/**
 * Created by IntelliJ IDEA.
 * User: AndreyB
 * Date: 11.10.11
 * Time: 11:08
 * To change this template use File | Settings | File Templates.
 */
package {
import flash.media.Camera;
import flash.media.Microphone;

//Предоставляет доступ к камере и микрофону
public class MediaDevicesSingleton {
    private static var _instance:MediaDevicesSingleton;

    private var _cam:Camera;
    private var _mike:Microphone;

    private const _fps:int = 24;
    private const _keyFrameInterval:int = 28;
    private const _mikeRate:int = 15;

    public static function getInstance():MediaDevicesSingleton {
        if (!_instance)
            _instance = new MediaDevicesSingleton(new SingletonEnforcer());
        return _instance;
    }

    public function MediaDevicesSingleton(enforcer:SingletonEnforcer) {
        initMediaDevices();
    }

    public function initMediaDevices():void {
        if (Camera.names.length > 0) {
            _cam = Camera.getCamera();
            camera.setMode(800, 600, _fps);
            camera.setKeyFrameInterval(_keyFrameInterval);
            camera.setQuality(40000, 85);
        }

        if (Microphone.names.length > 0) {
            _mike = Microphone.getMicrophone();
//            microphone.setSilenceLevel(0);
//            microphone.rate = _mikeRate;
        }
    }

    public function get camera():Camera {
        return _cam;
    }

    public function get microphone():Microphone {
        return _mike;
    }


}
}
class SingletonEnforcer {
}
